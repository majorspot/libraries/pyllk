import unittest
from contextlib import redirect_stdout
from io import StringIO

from pyllk.grammar import Grammar
from pyllk.parser import Context, Parser
from pyllk.production_rule import ProductionRule
from pyllk.token import NonTerminalToken, TerminalToken
from pyllk.token_stream import TokenStream


class MockDevice():
    """A mock device to temporarily suppress output to stdout
    Similar to UNIX /dev/null.
    """

    def write(self, s):
        pass


class ParserTest(unittest.TestCase):
    def test_constructor(self):
        p1 = ProductionRule(NonTerminalToken("parser"), [NonTerminalToken("X"), TerminalToken("Z")])
        p2 = ProductionRule(NonTerminalToken("A"), [NonTerminalToken("X"), TerminalToken("Y")])

        g = Grammar([p1, p2])

        p = Parser(g)

        self.assertEqual(g, p.grammar)

    def test_trivial_grammar(self):
        p = ProductionRule(NonTerminalToken("parser"), [TerminalToken("A"), TerminalToken("B"), TerminalToken("C")])
        g = Grammar([p])
        parser = Parser(g)

        input = [TerminalToken("A"), TerminalToken("B"), TerminalToken("C")]

        parser.consume_next_sub_rule(NonTerminalToken("parser"), TokenStream(input))

    def test_get_non_existant_production_rule(self):
        p1 = ProductionRule(NonTerminalToken("parser"), [NonTerminalToken("X"), TerminalToken("Z")])
        g = Grammar([p1])
        parser = Parser(g)

        try:
            parser.consume_next_sub_rule(NonTerminalToken("A"), None)
            self.fail("Should have thrown an exception")
        except Exception:
            pass

    def test_consume_next_sub_rule1(self):
        p1 = ProductionRule(NonTerminalToken("parser"), [TerminalToken("Z")])
        g = Grammar([p1])
        parser = Parser(g)

        self.assertIsNotNone(parser.consume_next_sub_rule(NonTerminalToken("parser"), TokenStream([TerminalToken("Z")])))

    def test_consume_next_sub_rule_should_fail1(self):
        p1 = ProductionRule(NonTerminalToken("parser"), [TerminalToken("X")])
        g = Grammar([p1])
        parser = Parser(g)

        self.assertIsNone(parser.consume_next_sub_rule(NonTerminalToken("parser"), TokenStream([TerminalToken("Z")])))

    def test_consume_next_sub_rule2(self):
        p1 = ProductionRule(NonTerminalToken("parser"), [NonTerminalToken("A"), TerminalToken("b")])
        p2 = ProductionRule(NonTerminalToken("A"), [TerminalToken("a")])
        g = Grammar([p1, p2])
        parser = Parser(g)

        self.assertIsNotNone(parser.consume_next_sub_rule(NonTerminalToken("parser"), TokenStream([TerminalToken("a"), TerminalToken("b")])))

    def test_consume_next_sub_rule3(self):
        p1 = ProductionRule(NonTerminalToken("parser"), [NonTerminalToken("A"), TerminalToken("b")])
        p2 = ProductionRule(NonTerminalToken("A"), [TerminalToken("a")])
        p3 = ProductionRule(NonTerminalToken("A"), [TerminalToken("c")])
        g = Grammar([p1, p2, p3])
        parser = Parser(g)

        self.assertIsNotNone(parser.consume_next_sub_rule(NonTerminalToken("parser"), TokenStream([TerminalToken("c"), TerminalToken("b")])))

    def test_consume_next_sub_rule4(self):
        p1 = ProductionRule(NonTerminalToken("parser"), [NonTerminalToken("A"), NonTerminalToken("B")])
        p2 = ProductionRule(NonTerminalToken("A"), [TerminalToken("a")])
        p3 = ProductionRule(NonTerminalToken("A"), [TerminalToken("c")])
        p4 = ProductionRule(NonTerminalToken("B"), [TerminalToken("b")])
        p5 = ProductionRule(NonTerminalToken("B"), [TerminalToken("d")])
        g = Grammar([p1, p2, p3, p4, p5])
        parser = Parser(g)

        self.assertIsNotNone(parser.consume_next_sub_rule(NonTerminalToken("parser"), TokenStream([TerminalToken("c"), TerminalToken("b")])))

    def test_consume_next_sub_rule5(self):
        p1 = ProductionRule(NonTerminalToken("parser"), [NonTerminalToken("A"), NonTerminalToken("B")])
        p2 = ProductionRule(NonTerminalToken("A"), [TerminalToken("a")])
        p3 = ProductionRule(NonTerminalToken("A"), [TerminalToken("c")])
        p4 = ProductionRule(NonTerminalToken("B"), [TerminalToken("b")])
        p5 = ProductionRule(NonTerminalToken("B"), [TerminalToken("d")])
        g = Grammar([p1, p2, p3, p4, p5])
        parser = Parser(g)

        self.assertIsNotNone(parser.consume_next_sub_rule(NonTerminalToken("parser"), TokenStream([TerminalToken("c"), TerminalToken("d")])))

    def test_consume_next_sub_rule6(self):
        p1 = ProductionRule(NonTerminalToken("parser"), [NonTerminalToken("A"), NonTerminalToken("B")])
        p2 = ProductionRule(NonTerminalToken("A"), [TerminalToken("a")])
        p3 = ProductionRule(NonTerminalToken("A"), [TerminalToken("c")])
        p4 = ProductionRule(NonTerminalToken("B"), [TerminalToken("b")])
        p5 = ProductionRule(NonTerminalToken("B"), [TerminalToken("d")])
        g = Grammar([p1, p2, p3, p4, p5])
        parser = Parser(g)

        self.assertIsNotNone(parser.consume_next_sub_rule(NonTerminalToken("parser"), TokenStream([TerminalToken("a"), TerminalToken("b")])))

    def test_consume_next_sub_rule7(self):
        p1 = ProductionRule(NonTerminalToken("parser"), [NonTerminalToken("A"), NonTerminalToken("B")])
        p2 = ProductionRule(NonTerminalToken("A"), [TerminalToken("a")])
        p3 = ProductionRule(NonTerminalToken("A"), [TerminalToken("c")])
        p4 = ProductionRule(NonTerminalToken("B"), [TerminalToken("b")])
        p5 = ProductionRule(NonTerminalToken("B"), [TerminalToken("d")])
        g = Grammar([p1, p2, p3, p4, p5])
        parser = Parser(g)

        self.assertIsNotNone(parser.consume_next_sub_rule(NonTerminalToken("parser"), TokenStream([TerminalToken("a"), TerminalToken("d")])))

    def test_consume_next_sub_rule8(self):
        p1 = ProductionRule(NonTerminalToken("parser"), [NonTerminalToken("A"), NonTerminalToken("B")])
        p2 = ProductionRule(NonTerminalToken("A"), [TerminalToken("a")])
        p3 = ProductionRule(NonTerminalToken("A"), [TerminalToken("c")])
        p4 = ProductionRule(NonTerminalToken("B"), [TerminalToken("b")])
        p5 = ProductionRule(NonTerminalToken("B"), [TerminalToken("d")])
        g = Grammar([p1, p2, p3, p4, p5])
        parser = Parser(g)

        self.assertIsNone(parser.consume_next_sub_rule(NonTerminalToken("parser"), TokenStream([TerminalToken("b"), TerminalToken("d")])))

    def test_consume_next_sub_rule9(self):
        p1 = ProductionRule(NonTerminalToken("parser"), [NonTerminalToken("A"), NonTerminalToken("B")])
        p2 = ProductionRule(NonTerminalToken("A"), [TerminalToken("a")])
        p3 = ProductionRule(NonTerminalToken("A"), [TerminalToken("c")])
        p4 = ProductionRule(NonTerminalToken("B"), [TerminalToken("b")])
        p5 = ProductionRule(NonTerminalToken("B"), [TerminalToken("d")])
        g = Grammar([p1, p2, p3, p4, p5])
        parser = Parser(g)

        self.assertIsNone(parser.consume_next_sub_rule(NonTerminalToken("parser"), TokenStream([TerminalToken("a"), TerminalToken("c")])))

    def test_parse_calculator_example1(self):
        g = Grammar(calculator_rules)
        parser = Parser(g)

        self.assertTrue(parser.parse_string("433/5"))

    def test_parse_calculator_example2(self):
        g = Grammar(calculator_rules)
        parser = Parser(g)

        self.assertTrue(parser.parse_string("(433/50)+3"))

    def test_parse_calculator_example3(self):
        g = Grammar(calculator_rules)
        parser = Parser(g)

        self.assertTrue(parser.parse_string("(433/50)+(3-1)"))

    def test_parse_calculator_fail1(self):
        g = Grammar(calculator_rules)
        parser = Parser(g)

        self.assertFalse(parser.parse_string("1+"))

    def test_parse_calculator_fail2(self):
        g = Grammar(calculator_rules)
        parser = Parser(g)

        self.assertFalse(parser.parse_string("+2"))

    def test_parse_calculator_fail3(self):
        g = Grammar(calculator_rules)
        parser = Parser(g)

        context = {
            'a': 3
        }
        self.assertFalse(parser.parse_string("3+", context))
        self.assertEqual(3, context['a'])

    def test_parse_calculator_with_context_example1(self):
        g = Grammar(calculator_rules_with_context)

        with redirect_stdout(StringIO()) as stdout:
            parser = Parser(g, debug=True)

            context = {
                'stack': []
            }
            self.assertTrue(parser.parse_string("1-1", context))
            self.assertEqual(0, context['stack'].pop())
            self.assertTrue(len(stdout.getvalue()) > 0)

    def test_parse_calculator_with_context_example2(self):
        g = Grammar(calculator_rules_with_context)
        parser = Parser(g)

        context = {
            'stack': []
        }
        self.assertTrue(parser.parse_string("((10+2)*15)/(7-2)", context))
        self.assertEqual(36, context['stack'].pop())

    def test_context(self):
        context = Context()
        context.x = 1
        self.assertEqual(1, context.x)
        del context.x
        try:
            print(context.x)
        except AttributeError:
            pass
        try:
            del context.y
        except AttributeError:
            pass


# A set of classic calculator production rules
calculator_rules = []
calculator_rules.append(ProductionRule(NonTerminalToken("parser"), [NonTerminalToken("expr"), TerminalToken("+"), NonTerminalToken("expr")]))
calculator_rules.append(ProductionRule(NonTerminalToken("parser"), [NonTerminalToken("expr"), TerminalToken("-"), NonTerminalToken("expr")]))
calculator_rules.append(ProductionRule(NonTerminalToken("parser"), [NonTerminalToken("expr"), TerminalToken("/"), NonTerminalToken("expr")]))
calculator_rules.append(ProductionRule(NonTerminalToken("parser"), [NonTerminalToken("expr"), TerminalToken("*"), NonTerminalToken("expr")]))
calculator_rules.append(ProductionRule(NonTerminalToken("expr"), [TerminalToken("("), NonTerminalToken("parser"), TerminalToken(")")]))
calculator_rules.append(ProductionRule(NonTerminalToken("expr"), [NonTerminalToken("number")]))
calculator_rules.append(ProductionRule(NonTerminalToken("number"), [TerminalToken("0"), NonTerminalToken("digit_or_empty")]))
calculator_rules.append(ProductionRule(NonTerminalToken("number"), [TerminalToken("1"), NonTerminalToken("digit_or_empty")]))
calculator_rules.append(ProductionRule(NonTerminalToken("number"), [TerminalToken("2"), NonTerminalToken("digit_or_empty")]))
calculator_rules.append(ProductionRule(NonTerminalToken("number"), [TerminalToken("3"), NonTerminalToken("digit_or_empty")]))
calculator_rules.append(ProductionRule(NonTerminalToken("number"), [TerminalToken("4"), NonTerminalToken("digit_or_empty")]))
calculator_rules.append(ProductionRule(NonTerminalToken("number"), [TerminalToken("5"), NonTerminalToken("digit_or_empty")]))
calculator_rules.append(ProductionRule(NonTerminalToken("number"), [TerminalToken("6"), NonTerminalToken("digit_or_empty")]))
calculator_rules.append(ProductionRule(NonTerminalToken("number"), [TerminalToken("7"), NonTerminalToken("digit_or_empty")]))
calculator_rules.append(ProductionRule(NonTerminalToken("number"), [TerminalToken("8"), NonTerminalToken("digit_or_empty")]))
calculator_rules.append(ProductionRule(NonTerminalToken("number"), [TerminalToken("9"), NonTerminalToken("digit_or_empty")]))
calculator_rules.append(ProductionRule(NonTerminalToken("digit_or_empty"), [TerminalToken("0"), NonTerminalToken("digit_or_empty")]))
calculator_rules.append(ProductionRule(NonTerminalToken("digit_or_empty"), [TerminalToken("1"), NonTerminalToken("digit_or_empty")]))
calculator_rules.append(ProductionRule(NonTerminalToken("digit_or_empty"), [TerminalToken("2"), NonTerminalToken("digit_or_empty")]))
calculator_rules.append(ProductionRule(NonTerminalToken("digit_or_empty"), [TerminalToken("3"), NonTerminalToken("digit_or_empty")]))
calculator_rules.append(ProductionRule(NonTerminalToken("digit_or_empty"), [TerminalToken("4"), NonTerminalToken("digit_or_empty")]))
calculator_rules.append(ProductionRule(NonTerminalToken("digit_or_empty"), [TerminalToken("5"), NonTerminalToken("digit_or_empty")]))
calculator_rules.append(ProductionRule(NonTerminalToken("digit_or_empty"), [TerminalToken("6"), NonTerminalToken("digit_or_empty")]))
calculator_rules.append(ProductionRule(NonTerminalToken("digit_or_empty"), [TerminalToken("7"), NonTerminalToken("digit_or_empty")]))
calculator_rules.append(ProductionRule(NonTerminalToken("digit_or_empty"), [TerminalToken("8"), NonTerminalToken("digit_or_empty")]))
calculator_rules.append(ProductionRule(NonTerminalToken("digit_or_empty"), [TerminalToken("9"), NonTerminalToken("digit_or_empty")]))
calculator_rules.append(ProductionRule(NonTerminalToken("digit_or_empty"), [TerminalToken("")]))

# A set of classic calculator production rules but with a context that computes the result


def action_make_number(ec):
    s = ""
    for token in ec.tokens:
        s = s + token.representation
    ec.context['stack'].append(float(s))


def add(e):
    b = e.context['stack'].pop()
    a = e.context['stack'].pop()
    e.parser.log("Ex: {} + {}".format(a, b))
    e.context['stack'].append(a + b)


def sub(e):
    b = e.context['stack'].pop()
    a = e.context['stack'].pop()
    e.parser.log("Ex: {} - {}".format(a, b))
    e.context['stack'].append(a - b)


def mul(e):
    b = e.context['stack'].pop()
    a = e.context['stack'].pop()
    e.parser.log("Ex: {} x {}".format(a, b))
    e.context['stack'].append(a * b)


def div(e):
    b = e.context['stack'].pop()
    a = e.context['stack'].pop()
    e.parser.log("Ex: {} / {}".format(a, b))
    e.context['stack'].append(a / b)


calculator_rules_with_context = []
calculator_rules_with_context.append(ProductionRule(NonTerminalToken("parser"), [NonTerminalToken("expr"), TerminalToken("+"), NonTerminalToken("expr")], add))
calculator_rules_with_context.append(ProductionRule(NonTerminalToken("parser"), [NonTerminalToken("expr"), TerminalToken("-"), NonTerminalToken("expr")], sub))
calculator_rules_with_context.append(ProductionRule(NonTerminalToken("parser"), [NonTerminalToken("expr"), TerminalToken("/"), NonTerminalToken("expr")], div))
calculator_rules_with_context.append(ProductionRule(NonTerminalToken("parser"), [NonTerminalToken("expr"), TerminalToken("*"), NonTerminalToken("expr")], mul))
calculator_rules_with_context.append(ProductionRule(NonTerminalToken("expr"), [TerminalToken("("), NonTerminalToken("parser"), TerminalToken(")")]))
calculator_rules_with_context.append(ProductionRule(NonTerminalToken("expr"), [NonTerminalToken("number")], action_make_number))
calculator_rules_with_context.append(ProductionRule(NonTerminalToken("number"), [TerminalToken("0"), NonTerminalToken("digit_or_empty")]))
calculator_rules_with_context.append(ProductionRule(NonTerminalToken("number"), [TerminalToken("1"), NonTerminalToken("digit_or_empty")]))
calculator_rules_with_context.append(ProductionRule(NonTerminalToken("number"), [TerminalToken("2"), NonTerminalToken("digit_or_empty")]))
calculator_rules_with_context.append(ProductionRule(NonTerminalToken("number"), [TerminalToken("3"), NonTerminalToken("digit_or_empty")]))
calculator_rules_with_context.append(ProductionRule(NonTerminalToken("number"), [TerminalToken("4"), NonTerminalToken("digit_or_empty")]))
calculator_rules_with_context.append(ProductionRule(NonTerminalToken("number"), [TerminalToken("5"), NonTerminalToken("digit_or_empty")]))
calculator_rules_with_context.append(ProductionRule(NonTerminalToken("number"), [TerminalToken("6"), NonTerminalToken("digit_or_empty")]))
calculator_rules_with_context.append(ProductionRule(NonTerminalToken("number"), [TerminalToken("7"), NonTerminalToken("digit_or_empty")]))
calculator_rules_with_context.append(ProductionRule(NonTerminalToken("number"), [TerminalToken("8"), NonTerminalToken("digit_or_empty")]))
calculator_rules_with_context.append(ProductionRule(NonTerminalToken("number"), [TerminalToken("9"), NonTerminalToken("digit_or_empty")]))
calculator_rules_with_context.append(ProductionRule(NonTerminalToken("digit_or_empty"), [TerminalToken("0"), NonTerminalToken("digit_or_empty")]))
calculator_rules_with_context.append(ProductionRule(NonTerminalToken("digit_or_empty"), [TerminalToken("2"), NonTerminalToken("digit_or_empty")]))
calculator_rules_with_context.append(ProductionRule(NonTerminalToken("digit_or_empty"), [TerminalToken("3"), NonTerminalToken("digit_or_empty")]))
calculator_rules_with_context.append(ProductionRule(NonTerminalToken("digit_or_empty"), [TerminalToken("4"), NonTerminalToken("digit_or_empty")]))
calculator_rules_with_context.append(ProductionRule(NonTerminalToken("digit_or_empty"), [TerminalToken("5"), NonTerminalToken("digit_or_empty")]))
calculator_rules_with_context.append(ProductionRule(NonTerminalToken("digit_or_empty"), [TerminalToken("6"), NonTerminalToken("digit_or_empty")]))
calculator_rules_with_context.append(ProductionRule(NonTerminalToken("digit_or_empty"), [TerminalToken("7"), NonTerminalToken("digit_or_empty")]))
calculator_rules_with_context.append(ProductionRule(NonTerminalToken("digit_or_empty"), [TerminalToken("8"), NonTerminalToken("digit_or_empty")]))
calculator_rules_with_context.append(ProductionRule(NonTerminalToken("digit_or_empty"), [TerminalToken("9"), NonTerminalToken("digit_or_empty")]))
calculator_rules_with_context.append(ProductionRule(NonTerminalToken("digit_or_empty"), [TerminalToken("")]))
