import unittest

from pyllk.grammar import Grammar
from pyllk.production_rule import ProductionRule
from pyllk.token import NonTerminalToken, TerminalToken


class GrammarTest(unittest.TestCase):
    def test_constructor(self):
        p1 = ProductionRule(NonTerminalToken("parser"), [NonTerminalToken("X"), TerminalToken("Z")])
        p2 = ProductionRule(NonTerminalToken("A"), [NonTerminalToken("X"), TerminalToken("Y")])
        g = Grammar([p1, p2])

        self.assertEqual(2, len(g.production_rules))
        self.assertEqual(p1, g.production_rules[0])
        self.assertEqual(p2, g.production_rules[1])

        self.assertEqual(1, len(g.lookup_table["parser"]))
        self.assertEqual(1, len(g.lookup_table["A"]))
        self.assertTrue("B" not in g.lookup_table)

    def test_constructor_without_init(self):
        p1 = ProductionRule(NonTerminalToken("A"), [NonTerminalToken("X"), TerminalToken("Z")])
        p2 = ProductionRule(NonTerminalToken("B"), [NonTerminalToken("X"), TerminalToken("Y")])

        try:
            Grammar([p1, p2])
            self.fail("Should have thrown an exception")
        except Exception:
            pass
