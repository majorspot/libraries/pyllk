import os
import re
import unittest
from pathlib import Path

import pyllk
from pyllk.parser import Context, Parser
from pyllk.token import TerminalToken


class CharacterTerminalToken(TerminalToken):
    def __init__(self, config):
        self.regex = f"^{config['regex']}$"
        super(CharacterTerminalToken, self).__init__(self.regex)

    def matches(self, obj):
        return True if re.search(self.regex, f"{obj}") else False


def character_tokenizer(definition: dict) -> CharacterTerminalToken:
    return CharacterTerminalToken(definition)


def make_action(definition: dict) -> None:
    def push_number(ec):
        s = ""
        for token in ec.tokens:
            s = s + token.representation
        ec.context['stack'].append(float(s))

    def add(e):
        b = e.context['stack'].pop()
        a = e.context['stack'].pop()
        e.parser.log("Ex: {} + {}".format(a, b))
        e.context['stack'].append(a + b)

    def sub(e):
        b = e.context['stack'].pop()
        a = e.context['stack'].pop()
        e.parser.log("Ex: {} - {}".format(a, b))
        e.context['stack'].append(a - b)

    def mul(e):
        b = e.context['stack'].pop()
        a = e.context['stack'].pop()
        e.parser.log("Ex: {} x {}".format(a, b))
        e.context['stack'].append(a * b)

    def div(e):
        b = e.context['stack'].pop()
        a = e.context['stack'].pop()
        e.parser.log("Ex: {} / {}".format(a, b))
        e.context['stack'].append(a / b)

    if definition["fn"] == 'push_number':
        return push_number
    elif definition["fn"] == 'add':
        return add
    elif definition["fn"] == 'sub':
        return sub
    elif definition["fn"] == 'mul':
        return mul
    elif definition["fn"] == 'div':
        return div
    else:
        return None


class ModuleTest(unittest.TestCase):
    def test_load(self):
        folder = Path(os.path.dirname(__file__)) / 'resources'
        path = folder / 'calculator.grammar'

        grammar = pyllk.load(path, token_fn=character_tokenizer, action_fn=make_action)

        context = Context()
        context.stack = []
        parser = Parser(grammar)

        self.assertTrue(parser.parse_string("((10+2)*15)/(7-2)", context))

        result = context.stack.pop()

        self.assertEqual(36, result)

    def test_load_and_parse_without_actions(self):
        folder = Path(os.path.dirname(__file__)) / 'resources'
        path = folder / 'calculator.grammar'

        grammar = pyllk.load(path, token_fn=character_tokenizer)

        parser = Parser(grammar)

        self.assertTrue(parser.parse_string("((10+2)*15)/(7-2)"))
        self.assertFalse(parser.parse_string("((x+2)*15)/(7-2)"))
        self.assertFalse(parser.parse_string("((10+2*15)/(7-2)"))
