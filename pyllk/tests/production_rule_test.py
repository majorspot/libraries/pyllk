import unittest

from pyllk.production_rule import ProductionRule
from pyllk.token import NonTerminalToken, TerminalToken


class ProductionRuleTest(unittest.TestCase):
    def test_constructor(self):
        source = NonTerminalToken("X")
        produced = [NonTerminalToken("Y"), TerminalToken("Z")]
        p = ProductionRule(source, produced)

        self.assertEqual(source, p.source)
        self.assertEqual(produced, p.produced)
