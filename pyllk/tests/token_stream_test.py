import unittest

from pyllk.token import TerminalToken
from pyllk.token_stream import TokenStream


class ParserTest(unittest.TestCase):

    def test_token_stream_basic(self):
        tks = TokenStream([TerminalToken("A"), TerminalToken("B"), TerminalToken("C")])

        self.assertEqual(3, len(tks.tokens))
        self.assertEqual(0, tks.head)

        self.assertEqual("A", tks.consume().representation)
        self.assertEqual(1, tks.head)
        self.assertEqual(2, len(tks.left_overs()))
        self.assertFalse(tks.is_drained())
        self.assertEqual("B", tks.consume().representation)
        self.assertEqual("C", tks.consume().representation)
        self.assertTrue(tks.is_drained())
        tks.set_to(2)
        self.assertFalse(tks.is_drained())
        self.assertEqual("C", tks.consume().representation)
        self.assertTrue(tks.is_drained())

        tks.set_to(0)
        self.assertEqual("A", tks.consume().representation)
        self.assertEqual(1, tks.head)
        self.assertEqual("B", tks.peek().representation)

        tks.set_to(2)
        self.assertEqual("C", tks.consume().representation)
        self.assertTrue(tks.is_drained())

    def test_token_stream_from_iterator(self):
        iteratable = [TerminalToken("A"), TerminalToken("B"), TerminalToken("C")]
        tks = TokenStream.from_iterator(iteratable)

        self.assertEqual(3, len(tks.tokens))
        self.assertEqual(0, tks.head)
