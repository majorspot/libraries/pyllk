import unittest

from pyllk.token import NonTerminalToken, TerminalToken


class TokenTest(unittest.TestCase):
    def test_non_terminal_token_name(self):
        x = NonTerminalToken("X")
        self.assertEqual("X", x.name)

        y = NonTerminalToken("Y")
        self.assertEqual("Y", y.name)

    def test_terminal_representation(self):
        x = TerminalToken('string representation')
        self.assertEqual("string representation", x.representation)

        y = TerminalToken({'a': 0, 'b': 'B'})
        self.assertEqual({'a': 0, 'b': 'B'}, y.representation)

        z = TerminalToken({'a': 0, 'b': 'B'})
        self.assertNotEqual({'a': 1, 'b': 'B'}, z.representation)
