import json
import os
import re
import unittest
from pathlib import Path

import pyllk.ebnf.decoder as decoder
from pyllk.parser import Context, Parser
from pyllk.token import NonTerminalToken, TerminalToken


class CharacterTerminalToken(TerminalToken):
    def __init__(self, config):
        self.regex = f"^{config['regex']}$"
        super(CharacterTerminalToken, self).__init__(self.regex)

    def matches(self, obj):
        return True if re.search(self.regex, f"{obj}") else False


def character_tokenizer(definition: dict) -> CharacterTerminalToken:
    return CharacterTerminalToken(definition)


def make_action(definition: dict) -> None:
    return None


class DecoderTest(unittest.TestCase):
    def test_basic_conversion(self):
        folder = Path(os.path.dirname(__file__)) / 'resources'
        path = folder / 'calculator.json'
        with open(path, 'r') as file:
            web_grammar = json.loads(file.read())

        g = decoder.decode(web_grammar, character_tokenizer, make_action)

        self.assertEqual(9, len(g.production_rules))

        # Get the first production rule for "number"
        # Should be "tokens": [ "DIGIT", "number" ]

        number = list(filter(lambda r: r.source.name == 'number', g.production_rules))[0]
        self.assertEqual(2, len(number.produced))

        self.assertIsInstance(number.produced[0], CharacterTerminalToken)
        self.assertIsInstance(number.produced[1], NonTerminalToken)

        self.assertTrue(number.produced[0].matches("4"))
        self.assertFalse(number.produced[0].matches("44"))
        self.assertFalse(number.produced[0].matches("x"))

        # Make sure that thing can actually parse

        context = Context()
        parser = Parser(g)
        self.assertTrue(parser.parse_string("((10+2)*15)/(7-2)", context))
        self.assertFalse(parser.parse_string("((x+2)*15)/(7-2)", context))
        self.assertFalse(parser.parse_string("((10+2*15)/(7-2)", context))
