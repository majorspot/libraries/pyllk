import json
import os
import unittest
import unittest.mock
from pathlib import Path
from unittest.mock import call, patch

import pyllk.ebnf.parser as parser


class ParserTest(unittest.TestCase):
    @patch('builtins.print')
    def testillegal_character(self, mocked_print):
        lexer = parser.run_lex("import @, FLOAT from './test02.grammar'")

        expected_tokens = [
            ('IMPORT', 'import'),
            ('COMMA', ','),
            ('NAME', 'FLOAT'),
            ('FROM', 'from'),
            ('STRING', './test02.grammar'),
        ]

        for expected in expected_tokens:
            received = lexer.token()
            self.assertEqual(expected[0], received.type)
            self.assertEqual(expected[1], received.value)

        mocked_print.assert_has_calls([call("Illegal character '@'")])

    def test_parse_grammar(self):
        lexer = parser.run_lex("import number, FLOAT from './test02.grammar'")

        expected_tokens = [
            ('IMPORT', 'import'),
            ('NAME', 'number'),
            ('COMMA', ','),
            ('NAME', 'FLOAT'),
            ('FROM', 'from'),
            ('STRING', './test02.grammar'),
        ]

        for expected in expected_tokens:
            received = lexer.token()
            self.assertEqual(expected[0], received.type)
            self.assertEqual(expected[1], received.value)

    # def test_tmp(self):
    #     suffix = '06'
    #     result = parser.parse(os.path.join(os.path.dirname(__file__), f'test{suffix}.grammar'))
    #     print(json.dumps(result, indent=4))

    def evaluate_parse_grammar(self, suffix, debug=False):
        folder = Path(os.path.dirname(__file__)) / 'resources'
        result = parser.parse(folder / f'test{suffix}.grammar', debug=debug)
        with open(folder / f'expected{suffix}.json', 'r') as file:
            expected = json.loads(file.read())

        if expected != result:
            print(json.dumps(result, indent=4))

        return (expected, result)

    def test_parse_grammar01(self):
        result = self.evaluate_parse_grammar('01', debug=False)
        self.assertEqual(result[0], result[1])

    def test_parse_grammar02(self):
        result = self.evaluate_parse_grammar('02')
        self.assertEqual(result[0], result[1])

    def test_parse_grammar03(self):
        result = self.evaluate_parse_grammar('03')
        self.assertEqual(result[0], result[1])

    @patch('builtins.print')
    def test_parse_grammar04(self, mocked_print):
        folder = Path(os.path.dirname(__file__)).absolute() / 'resources'

        suffix = '04'
        path = folder / f'test{suffix}.grammar'

        parser.parse(path)
        mocked_print.assert_has_calls([call("Syntax error in input! LexToken(COMMENT,'# This should blow up\\n',1,56)")])

    def test_parse_grammar05(self):
        result = self.evaluate_parse_grammar('05')
        self.assertEqual(result[0], result[1])

    def test_parse_grammar09(self):
        result = self.evaluate_parse_grammar('09')
        self.assertEqual(result[0], result[1])
