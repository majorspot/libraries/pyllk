import json
import os
import unittest
from pathlib import Path

import pyllk.ebnf.parser as parser
import pyllk.ebnf.resolver as resolver
from pyllk.ebnf.resolver import ImportException


class ResolverTest(unittest.TestCase):
    def test_resolution01(self):
        folder = Path(os.path.dirname(__file__)) / 'resources'
        path = folder / 'test06.grammar'

        result = parser.parse(path)

        result = resolver.resolve(result, path)

        with open(folder / 'resolved06.json', 'r') as file:
            expected = json.loads(file.read())

        if expected != result:
            print(json.dumps(result, indent=4))

        self.assertEqual(expected, result)

    def test_resolution_with_terminal_conflict(self):
        folder = Path(os.path.dirname(__file__)) / 'resources'
        path = folder / 'test06_terminal_conflict.grammar'

        result = parser.parse(path)
        self.assertRaises(ImportException, resolver.resolve, result, path)

    def test_resolution_with_non_terminal_conflict(self):
        folder = Path(os.path.dirname(__file__)) / 'resources'
        path = folder / 'test06_non_terminal_conflict.grammar'

        result = parser.parse(path)
        self.assertRaises(ImportException, resolver.resolve, result, path)

    def test_import_cycle(self):
        folder = Path(os.path.dirname(__file__)) / 'resources'
        path = folder / 'import_cycle_01.grammar'
        result = parser.parse(path)
        self.assertRaises(ImportException, resolver.resolve, result, path)
